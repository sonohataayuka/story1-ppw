from django.urls import path
from . import views

app_name = 'HOMEPAGE'

urlpatterns = [
    path('', views.index, name='index'),
    # path('url-yang-diinginkan/', views.nama_fungsi, name='nama_fungsi')
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]